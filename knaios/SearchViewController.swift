//
//  SearchViewController.swift
//  knaios
//
//  Created by Andras Balogh on 10/30/17.
//  Copyright © 2017 iOS Mobile Computing. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var SocialWorkBtn: UIButton!
    @IBOutlet weak var InsurBtn: UIButton!
    @IBOutlet weak var MentalBtn: UIButton!
    @IBOutlet weak var RehabBtn: UIButton!
    @IBOutlet weak var FoodBtn: UIButton!
    @IBOutlet weak var HomelessBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let borderAlpha : CGFloat = 0.7
        //let cornerRadius : CGFloat = 5.0
        
        //button.setTitle("Get Started", forState: UIControlState.Normal)
        //button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        //button.backgroundColor = UIColor.clearColor()
        SocialWorkBtn.layer.borderWidth = 1.0
        SocialWorkBtn.layer.cornerRadius = 5.0
        SocialWorkBtn.layer.borderColor = UIColor.blue.cgColor
        InsurBtn.layer.borderWidth = 1.0
        InsurBtn.layer.cornerRadius = 5.0
        InsurBtn.layer.borderColor = UIColor.blue.cgColor
        MentalBtn.layer.borderWidth = 1.0
        MentalBtn.layer.cornerRadius = 5.0
        MentalBtn.layer.borderColor = UIColor.blue.cgColor
        RehabBtn.layer.borderWidth = 1.0
        RehabBtn.layer.cornerRadius = 5.0
        RehabBtn.layer.borderColor = UIColor.blue.cgColor
        FoodBtn.layer.borderWidth = 1.0
        FoodBtn.layer.cornerRadius = 5.0
        FoodBtn.layer.borderColor = UIColor.blue.cgColor
        HomelessBtn.layer.borderWidth = 1.0
        HomelessBtn.layer.cornerRadius = 5.0
        HomelessBtn.layer.borderColor = UIColor.blue.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
