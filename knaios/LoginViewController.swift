//
//  knaios
//
//  Created by Andras Balogh on 10/28/17.
//  Copyright © 2017 iOS Mobile Computing. All rights reserved.
//
//
//  LoginViewController.swift
//
//  Created by Nick Aguirre on 10/27/17.
//  Copyright © 2017 Nick. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usrName: UITextField!
    
    @IBOutlet weak var pass: UITextField!
    var alert = UIAlertController()
    
    @IBAction func loginButton(_ sender: UIButton) {
        if self.usrName.text == "" || self.pass.text == ""{
            self.alert = UIAlertController(title: "Error", message: "Both Username and Password must have values.", preferredStyle: UIAlertControllerStyle.alert)
            let ok = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
            }
            self.alert.addAction(ok)
            self.present(self.alert, animated: true, completion:nil)
        }
        else {
            let user = User(usr: usrName.text!, pass: pass.text!, email: "")
            let canLogin = DataStore.shared.login(user: user)
            if !canLogin{
                self.alert = UIAlertController(title: "Error", message: "One of your fields (Username/Password) is incorrect.", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
                }
                self.alert.addAction(ok)
                self.present(self.alert, animated: true, completion:nil)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        DataStore.shared.loadPeople()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        usrName.resignFirstResponder()
        pass.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}



