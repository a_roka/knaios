//
//  User.swift
//  mockupPaper
//
//  Created by Nick Aguirre on 10/27/17.
//  Copyright © 2017 Nick. All rights reserved.
//

import Foundation

class User {
    
    var username: String
    var password: String
    var email: String
    
    init(usr: String, pass: String, email: String) {
        self.username = usr
        self.password = pass
        self.email = email
    }
}

