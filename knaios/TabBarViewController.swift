//
//  TabBarViewController.swift
//  knaios
//
//  Created by Nick Aguirre on 10/31/17.
//  Copyright © 2017 iOS Mobile Computing. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
